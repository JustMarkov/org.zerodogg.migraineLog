# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.12.0] - Unreleased

### Added
- You can now keep track of when you took your medication

### Fixed
- Slow changing of dates in the calendar in certain cases

### Changed
- Data format is now version 4.

## [0.11.3] - 2024-08-18

### Fixed
- A bug that could cause Migraine Log to hang when deleting an entry

### Changed
- Android only: updated target SDK to 34

## [0.11.2] - 2024-03-19

### Fixed
- "No headache" days mistakenly being treated as headache days

## [0.11.1] - 2024-02-16

### Fixed
- Building without the "ios" directory (for F-droid)

## [0.11.0] - 2024-02-11

### Added
- The ability to enable a feature that lets you add notes to days without
    attacks
- European Portuguese and Russian translations

### Changed
- Updated Flutter to 3.16
- Improved UI adaptation on iOS

### Fixed
- Rendering issues on certain LineageOS devices

## [0.10.5] - 2024-01-27

### Changed
- Updated translations for Basque and German
- Use VectorDrawable icons on Android (thanks Sébastien Delord)
- Added monochrome icon on Android (thanks Sébastien Delord)

## [0.10.4] - 2023-12-15

### Fixed
- An invalid configuration file will no longer make Migraine Log hang at startup

## [0.10.3] - 2023-11-05

### Fixed
- Statistics calculations in months where daylight savings time has changed

## [0.10.2] - 2023-09-24

### Changed
- Android only: fixed a bug that caused importing not to work on Android 13

## [0.10.1] - 2023-08-27

### Changed
- Android only: update target SDK to 33
- Fixed various typos

## [0.10.0] - 2023-03-04

### Changed
- Updated Flutter to 3.3.10

### Added
- Basque translation

## [0.9.1] - 2022-09-18

### Added
- French translation

## [0.9.0] - 2022-08-07

### Changed
- Updated to flutter version 3
- Tweaked calendar layout on large (tablet) displays

### Added
- iOS version

## [0.8.4] - 2022-07-30

### Changed
- Added Chinese (hans) translation

## [0.8.3] - 2022-07-21

### Changed
- Updated German translation

## [0.8.2] - 2022-07-19

### Fixed
- English language preferences now honored
- Exported html now uses proper grammar for dates in Finnish

## [0.8.1] - 2022-06-16

### Fixed
- F-droid builds

## [0.8.0] - 2022-06-12

### Added
- You may now optionally enable registration of attacks where you only had aura-symptoms

### Changed
- Data format version is now 2. It can read version 1 files, but importing data from this version will require Migraine Log 0.8.0 or later.

## [0.7.2] - 2021-06-09

### Fixed
- An issue where Migraine Log might not save data if the only change that has been made is deleting an entry

## [0.7.1] - 2021-05-20

### Changed
- Updated Spanish translation

## [0.7.0] - 2021-05-09

### Added
- Can now show an indicator on dates where you have added a note
- You may now toggle showing dates where there are no entries in the exported HTML-file

## [0.6.1] - 2021-04-07

### Changed
- Updated German translation

### Fixed
- Corrected error messages when importing data from a later version of Migraine Log

## [0.6.0] - 2021-03-23

### Changed
- Can now recover if data should get corrupted (ie. if the battery is depleted while saving)
- Fixes to ease translation
- Added a German translation by jofrev
- Added a Spanish translation by Diego

## [0.5.1] - 2021-03-18

### Fixed
- It is now possible to scroll in the add/edit screen so that the save-button no longer overlaps the notes field
- Translation fixes for Norwegian

## [0.5.0] - 2021-03-16

### Added
- A help dialog that explains the various strengths

### Changed
- The icon is now provided as an android adaptive icon, this should resolve issues with the icon in certain launchers

## [0.4.0] - 2021-03-08

### Changed
- Long pressing a date in the calendar will now add or edit an entry on that date
- Credit translators in the about dialog
- Various translation updates
- Table headers in exported files will now repeat when printing if a table spans more than one page
- Code cleanup

## [0.3.1] - 2021-03-06

### Added
- Finnish translation by Mika Latvala
- Metadata required for f-droid

## [0.3.0] - 2021-03-02

### Added
- Support for hiding and showing individual months in the exported file
- Support for sorting the tables in the exported file
- Support for limiting the number of months displayed in the exported file

### Fixed
- The import success message can now be translated

## [0.2.0] - 2021-02-27

### Added
- Support for importing data from exported files

### Changed
- Exported data is now section into monthly tables
- Exported data now contains summary lines, like those found in the stats tab

### Fixed
- Tooltips for entries in the tab bar now translatable

## [0.1.2] - 2021-02-24

### Fixed
- Fixed the "help"-window

## [0.1.1] - 2021-02-23

### Added
- Added links to the about dialog

### Fixed
- Fixed the ability to translate the screen reader text for the current month

## [0.1.0] - 2021-02-23
- Initial release
