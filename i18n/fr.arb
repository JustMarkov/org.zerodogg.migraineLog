{
  "@@last_modified": "2025-01-19T19:53:11.738830",
  "@@locale": "fr",
  "General": "Général",
  "@General": {
    "description": "Used as a header in the config screen to refer to \"general settings\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Continue": "Continuer",
  "@Continue": {
    "description": "Used as a button in the 'first time' configuration screen",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Use neutral language": "Utiliser un language neutre",
  "@Use neutral language": {
    "description": "Toggle button that enables (or disables) use of \"headache\" instead of \"migraine\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Use the term \"headache\" rather than \"migraine\"": "Utilise le terme \"mal de tête\" au lieu de \"migraine\"",
  "@Use the term \"headache\" rather than \"migraine\"": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Enable \"aura only\" strength": "Active l'intensité \"uniquement l'aura\"",
  "@Enable \"aura only\" strength": {
    "description": "Toggle button that enables (or disables) use of \"aura only\" as a \"strength\" option",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Let me set \"aura only\" as a the strength of an attack": "Permet de mettre \"uniquement l'aura\" comme intensité d'une crise",
  "@Let me set \"aura only\" as a the strength of an attack": {
    "description": "This is used as a description for the 'aura only' config option. Feel free to rephrase.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Show note indicators": "Affiche les indicateurs de note",
  "@Show note indicators": {
    "description": "Toggle button that enables/disables showing a marker on dates with notes in the calendar",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Show a small dot on dates where you have written a note": "Affiche un petit point aux dates où vous avez écrit une note",
  "@Show a small dot on dates where you have written a note": {
    "description": "The 'dot' is a small grey square, displayed in the upper right hand corner of the date number in the calendar",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "What kind of headache do you have?": "Quel type de maux de tête avez vous?",
  "@What kind of headache do you have?": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Select this if you're unsure": "Sélectionnez ceci si vous n'êtes pas sûr",
  "@Select this if you're unsure": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine": "Migraine",
  "@Migraine": {
    "description": "Used as level 2/3 (medium) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Other (general headaches, cluster etc.)": "Autre (céphalée générale, algie vasculaire de la face, etc.)",
  "@Other (general headaches, cluster etc.)": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Settings": "Paramètres",
  "@Settings": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Configuration": "Configuration",
  "@Configuration": {
    "description": "The name of the first time configuration screen. This is pretty much like the settings screen, but with some different information and displayed only once, therefore using a different name to make this clearer.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medication list": "Liste de médicaments",
  "@Medication list": {
    "description": "Header for the part of the configuration screen where the user sets up their medication",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Changing the list of medications here will not change medications in entries you have already added.": "Changer la liste de médicaments ici ne changera pas les médicaments dans les entrées que vous avez déjà ajoutées.",
  "@Changing the list of medications here will not change medications in entries you have already added.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "If you do not wish to add your medications, just press \"Continue\" and Migraine Log will add two generic ones for you to use.": "Si vous ne souhaitez pas ajouter vos médicaments, appuyez simplement sur \"Continuer\" et Journal de la Migraine ajoutera deux dénominations de médicament génériques que vous pourrez utiliser.",
  "@If you do not wish to add your medications, just press \"Continue\" and Migraine Log will add two generic ones for you to use.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Add a new medication": "Ajouter un nouveau médicament",
  "@Add a new medication": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Undo": "Annuler",
  "@Undo": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "_deletedElement": "\"{medication}\" effacé",
  "@_deletedElement": {
    "description": "A pop-up message after the user has deleted a medication. Is accompanied with an 'undo' button.",
    "type": "text",
    "placeholders_order": [
      "medication"
    ],
    "placeholders": {
      "medication": {}
    }
  },
  "No registration": "Pas d'enregistrement",
  "@No registration": {
    "description": "Used in the pie diagram and table on the front page to denote days where no entries have been added. Avoid terminology like 'no headaches', since that might not be quite correct.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Headache": "Mal de tête",
  "@Headache": {
    "description": "Used as level 1/3 (lowest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Mild headache": "Mal de tête léger",
  "@Mild headache": {
    "description": "Used as neutral level 1/3 (lowest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Moderate headache": "Mal de tête modéré",
  "@Moderate headache": {
    "description": "Used as neutral level 2/3 (medium) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Strong migraine": "Forte migraine",
  "@Strong migraine": {
    "description": "Used as level 3/3 (strongest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Strong headache": "Fort mal de tête",
  "@Strong headache": {
    "description": "Used as neutral level 3/3 (strongest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Aura only": "Uniquement l'aura",
  "@Aura only": {
    "description": "Used as the 'strength' of a migraine or headache where the user only had aura symptoms",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine medication": "Médicament pour la migraine",
  "@Migraine medication": {
    "description": "Used as a generic 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Seizure medication": "Médicament antiépileptique",
  "@Seizure medication": {
    "description": "Used as neutral generic 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Painkillers": "Antidouleurs",
  "@Painkillers": {
    "description": "Used as the second 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Edit": "Modifier",
  "@Edit": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Register": "Enregister",
  "@Register": {
    "description": "Displayed as the header when registering a new migraine attack",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "You must select a strength": "Il faut sélectionner une intensité",
  "@You must select a strength": {
    "description": "Notice that appears if the user tries to save an entry without selecting a strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Save": "Sauvegarder",
  "@Save": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "editingEntryNewDate": "Vous modifiez une entrée au {editDate}. Si vous changez la date, cette entrée sera déplacée à la nouvelle date.",
  "@editingEntryNewDate": {
    "type": "text",
    "placeholders_order": [
      "editDate"
    ],
    "placeholders": {
      "editDate": {}
    }
  },
  "Warning: there's already an entry on this date. If you save then the existing entry will be overwritten.": "Attention: Il y a déjà une entrée à cette date. Si vous enregistrez, l'entrée existante sera écrasée.",
  "@Warning: there's already an entry on this date. If you save then the existing entry will be overwritten.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Date": "Date",
  "@Date": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "change date": "changer la date",
  "@change date": {
    "description": "Used as an action label on the button that triggers a calendar to select the date for an attack when a screen reader is used. It will be contextualized by the OS, on Android the OS will say the label of the button, then the word 'button', followed by 'double tap to change date'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Strength": "Intensité",
  "@Strength": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Pain is very subjective. The only person that knows which option is right, is you. If you're uncertain, select the higher one of the two you're considering.": "La douleur est très subjective. La seule personne qui sait quelle option est la bonne, c'est vous. Si vous n'êtes pas sûr, choisissez la plus élevée des deux options que vous envisagez.",
  "@Pain is very subjective. The only person that knows which option is right, is you. If you're uncertain, select the higher one of the two you're considering.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Unable to perform most activities": "Incapable d'effectuer la plupart des activités",
  "@Unable to perform most activities": {
    "description": "Migraine strength 3/3",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Unable to perform some activities": "Incapable d'effectuer certaines activités",
  "@Unable to perform some activities": {
    "description": "Migraine strength 2/3",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Able to perform most activities": "Capable d'effectuer la plupart des activités",
  "@Able to perform most activities": {
    "description": "Migraine strength 1/3",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Aura symptoms only": "Uniquement les symptômes de l'aura",
  "@Aura symptoms only": {
    "description": "Migraine strength help text for aura only",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Close": "Fermer",
  "@Close": {
    "description": "Used as a 'close' button in the help dialog for migraine strength. This will be converted to upper case when used in this context, ie. 'Close' becomes 'CLOSE'. This to fit with Android UI guidelines",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Which strength should I choose?": "Quelle intensité dois-je choisir?",
  "@Which strength should I choose?": {
    "description": "Used as the tooltip for the help button in the 'add' and 'edit' screens as well as the title of the help window that pops up when this button is pressed.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medications taken": "Médicaments pris",
  "@Medications taken": {
    "description": "Header in the add/edit window",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Note": "Note",
  "@Note": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "(none)": "(aucun)",
  "@(none)": {
    "description": "Used in exported HTML when the user has taken no medications. It is an entry in a table where the header is 'Medications'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medications": "Médicaments",
  "@Medications": {
    "description": "Used as a table header in exported data",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine Log": "Journal de la Migraine",
  "@Migraine Log": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "hide": "cacher",
  "@hide": {
    "description": "Used in exported HTML as a link that hides one month. Will be displayed like '[hide]', so it should be lower case unless there are good reasons not to",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Hide this month": "Cacher ce mois",
  "@Hide this month": {
    "description": "Tooltip for a link that hides a month from view in the exported HTML",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "hidden:": "caché:",
  "@hidden:": {
    "description": "Will be rendered like: \"hidden: january 2021\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Total headache days": "Total des jours de maux de tête",
  "@Total headache days": {
    "description": "Used in a table, will render like 'Total headache days    20 days'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Taken medication": "Médicaments pris",
  "@Taken medication": {
    "description": "Used in a table, will render like 'Taken Medication    5 days'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Filter:": "Filtrer:",
  "@Filter:": {
    "description": "Will be joined with the string 'show' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "show": "montrer",
  "@show": {
    "description": "Will be joined with the string 'Filter:' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "everything": "tous",
  "@everything": {
    "description": "Will be joined with the string 'Filter: show' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "months": "mois",
  "@months": {
    "description": "Will be joined with a number, which will be 3 or higher, to become ie. '3 months'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "List days with no entries": "Lister les jours sans entrées",
  "@List days with no entries": {
    "description": "Checkbox, toggles showing or hiding (default=hide) dates that have no registered headaches in the tables in our exported HTML file",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "_generatedMessage": "Généré par Journal de la Migraine version {version}",
  "@_generatedMessage": {
    "type": "text",
    "placeholders_order": [
      "version"
    ],
    "placeholders": {
      "version": {}
    }
  },
  "Loading...": "Chargement...",
  "@Loading...": {
    "description": "Used in exported HTML to indicate that we're loading the exported data",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Previous month": "Mois précédent",
  "@Previous month": {
    "description": "Tooltip for the previous month button",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Next month": "Mois suivant",
  "@Next month": {
    "description": "Tooltip for the next month button",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "_currentMonth": "Mois en cours: {month}",
  "@_currentMonth": {
    "description": "Used for screen readers to describe the element in the month selector that indicates the current month. The month variable will contain a pre-localized month name along with the year",
    "type": "text",
    "placeholders_order": [
      "month"
    ],
    "placeholders": {
      "month": {}
    }
  },
  "Migraine Log helps you maintain a headache diary. Once you have added an entry, this screen will be replaced with statistics.\n\nAdd a new entry by pressing the \"+\"-button.\n\nFor more help, select \"Help\" in the menu at the top right of the screen.": "Journal de la Migraine vous aide à tenir un journal des maux de tête. Une fois que vous aurez ajouté une entrée, cet écran sera remplacé par les statistiques.\n\nAjoutez une nouvelle entrée en appuyant sur le bouton \"+\".\n\nPour plus d'aide, sélectionnez \"Aide\" dans le menu en haut à droite de l'écran.",
  "@Migraine Log helps you maintain a headache diary. Once you have added an entry, this screen will be replaced with statistics.\n\nAdd a new entry by pressing the \"+\"-button.\n\nFor more help, select \"Help\" in the menu at the top right of the screen.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Help": "Aide",
  "@Help": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Icons in the calendar": "Icônes dans le calendrier",
  "@Icons in the calendar": {
    "description": "Header in the help screen",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Colours": "Couleurs",
  "@Colours": {
    "description": "Header in the help screen",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "An underline under a day in the calendar means that there is a registered entry on that date, but that no medication was taken.": "Un soulignement sous un jour du calendrier signifie qu'il y a une entrée enregistrée à cette date, mais qu'aucun médicament n'a été pris.",
  "@An underline under a day in the calendar means that there is a registered entry on that date, but that no medication was taken.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "A circle around a day in the calendar means that there is a registered entry on that date, and that medication was taken.": "Un cercle autour d'un jour du calendrier signifie qu'il y a une entrée enregistrée à cette date et que des médicaments ont été pris.",
  "@A circle around a day in the calendar means that there is a registered entry on that date, and that medication was taken.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "A dot in the upper right hand corner of a date indicates that the entry on that date includes a note.": "Un point dans le coin supérieur droit d'une date indique que l'entrée à cette date comprend une note.",
  "@A dot in the upper right hand corner of a date indicates that the entry on that date includes a note.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Throughout Migraine Log, colours are used to indicate the strength of a migraine. The underline/circle in the calendar, as well as the text of the strength when adding or editing an entry, will use a colour to signify the strength.": "Dans tout le Journal de la Migraine, des couleurs sont utilisées pour indiquer l'intensité d'une migraine. Le soulignement/cercle dans le calendrier, ainsi que le texte de l'intensité lors de l'ajout ou de la modification d'une entrée, utiliseront une couleur pour signifier l'intensité.",
  "@Throughout Migraine Log, colours are used to indicate the strength of a migraine. The underline/circle in the calendar, as well as the text of the strength when adding or editing an entry, will use a colour to signify the strength.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "dayString": "{number,plural, =1{{number} jour}=2{{number} jours}other{{number} jours}}",
  "@dayString": {
    "description": "How many days there are. This isn't as complicated as it looks. Unless you know you need to modify the pluralization rules (in which case see https://pub.dev/documentation/intl/latest/intl/Intl/plural.html) you should just change the string 'day' and 'days' here into your local equivalent. For instance, the Norwegian version of this string is: '{number,plural, =1{{number} dag}=2{{number} dagar}other{{number} dagar}}'. number will always be a positive integer (never zero).",
    "type": "text",
    "placeholders_order": [
      "number"
    ],
    "placeholders": {
      "number": {}
    }
  },
  "Successfully imported data": "Données importées avec succès",
  "@Successfully imported data": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "The file is corrupt and can not be imported": "Ce fichier est corrompu et ne peut pas être importé",
  "@The file is corrupt and can not be imported": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "This file does not appear to be a Migraine Log (html) file": "Ce fichier ne semble pas être un fichier (html) Journal de la Migraine",
  "@This file does not appear to be a Migraine Log (html) file": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "This file is from a newer version of Migraine Log. Upgrade the app first.": "Ce fichier provient d'une version plus récente de Journal de la Migraine. Mettez d'abord l'application à jour.",
  "@This file is from a newer version of Migraine Log. Upgrade the app first.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "An unknown error occurred during import.": "Une erreur inconnue est survenue durant l'importation.",
  "@An unknown error occurred during import.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Import failed:": "Échec de l'importation:",
  "@Import failed:": {
    "description": "Will contain information about why after the :",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Your Migraine Log version is too old to read the data you have. Please upgrade Migraine Log.": "Votre version de Journal de la Migraine est trop ancienne pour lire les données que vous avez. Veuillez mettre à jour Journal de la Migraine.",
  "@Your Migraine Log version is too old to read the data you have. Please upgrade Migraine Log.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.": "Journal de la Migraine est un logiciel libre : vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence Publique Générale GNU telle que publiée par la Free Software Foundation, soit la version 3 de la Licence, soit (à votre choix) toute version ultérieure.\n\nCe programme est distribué dans l'espoir qu'il sera utile mais SANS AUCUNE GARANTIE; sans même la garantie implicite de COMMERCIALISATION ou de CONFORMITÉ À UN USAGE PARTICULIER.",
  "@Migraine Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Copyright ©": "Droits d'auteur ©",
  "@Copyright ©": {
    "description": "Will be followed by the author name and copyright year, which will be a link",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "See the": "Voir la",
  "@See the": {
    "description": "Used to construct the sentence 'See the GNU General Public License for details', where 'GNU General Public License' will be a link. Spacing around 'GNU General Public License' is automatic.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "for details.": "pour plus de détails.",
  "@for details.": {
    "description": "Used to construct the sentence 'See the GNU General Public License for details', where 'GNU General Public License' will be a link. Spacing around 'GNU General Public License' is automatic.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "About": "À propos",
  "@About": {
    "description": "Menu entry for triggering the about dialog",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Exit": "Sortir",
  "@Exit": {
    "description": "Used to exit the app",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Export": "Exporter",
  "@Export": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Import": "Importer",
  "@Import": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Statistics": "Statistiques",
  "@Statistics": {
    "description": "Tooltip for the statistics tab",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Calendar": "Calendrier",
  "@Calendar": {
    "description": "Tooltip for the calendar tab",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Home": "Accueil",
  "@Home": {
    "description": "Tooltip for the home tab",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Translated by": "Traduit par",
  "@Translated by": {
    "description": "Should be a literal translation of this phrase, the value of TRANSLATED_BY will be appended to make a string like this: \"Translated by: TRANSLATED_BY\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "TRANSLATED_BY": "Valanna",
  "@TRANSLATED_BY": {
    "description": "Should be an alphabetical list of the names of the translators of this language, delimited with commas or the localized equivalent of \"and\". Will be displayed in the about dialog like this: \"Translated by: TRANSLATED_BY\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "deletedDateMessage": "Effacé {fmtDate}",
  "@deletedDateMessage": {
    "type": "text",
    "placeholders_order": [
      "fmtDate"
    ],
    "placeholders": {
      "fmtDate": {}
    }
  },
  "Delete": "Effacer",
  "@Delete": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Add": "Ajouter",
  "@Add": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Show action menu": "Afficher le menu d'action",
  "@Show action menu": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "lastNDaysMessage": "{days} derniers jours",
  "@lastNDaysMessage": {
    "description": "days should always be >1, so plural",
    "type": "text",
    "placeholders_order": [
      "days"
    ],
    "placeholders": {
      "days": {}
    }
  },
  "took no medication": "aucun médicament pris",
  "@took no medication": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "tookMedsMessage": "pris {meds}",
  "@tookMedsMessage": {
    "description": "This will expand into a list of medications taken, and will be added to the type of headache the user had. For instance, this might get 'Imigran' as meds, this will then be 'took Imigran', and it might expand into the sentece 'Migraine, took Imigran'",
    "type": "text",
    "placeholders_order": [
      "meds"
    ],
    "placeholders": {
      "meds": {}
    }
  },
  "Note:": "Note:",
  "@Note:": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Your settings file failed to load. Your data is safe, and you may reset your settings to fix this issue (you will need to enter your medications again).": "Your settings file failed to load. Your data is safe, and you may reset your settings to fix this issue (you will need to enter your medications again).",
  "@Your settings file failed to load. Your data is safe, and you may reset your settings to fix this issue (you will need to enter your medications again).": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "If you want to report this issue, take a screenshot of the following information:": "If you want to report this issue, take a screenshot of the following information:",
  "@If you want to report this issue, take a screenshot of the following information:": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Reset settings": "Reset settings",
  "@Reset settings": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Error": "Error",
  "@Error": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Enable notes on days without attacks": "Enable notes on days without attacks",
  "@Enable notes on days without attacks": {
    "description": "Toggle button that enables (or disables) use of \"no headache\" as a \"strength\" option",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Lets you choose a \"no headache\" as the strength of an attack": "Lets you choose a \"no headache\" as the strength of an attack",
  "@Lets you choose a \"no headache\" as the strength of an attack": {
    "description": "This is used as a description for the 'no headache' config option. Feel free to rephrase.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "No headache": "No headache",
  "@No headache": {
    "description": "Used as the 'strength' of a day where the user had no headache (for registering notes on days without symptoms)",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Register medications": "Register medications",
  "@Register medications": {
    "description": "Header in the add/edit window",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medication": "Medication",
  "@Medication": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Taken": "Taken",
  "@Taken": {
    "description": "This is used in the header of the 'list of medications' table in the viewer. The column it refers to will contain the time that a medication was taken",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "takenAt": "Took {medication} at {time}",
  "@takenAt": {
    "description": "medication is the name of one of the users medications, time is a time in either 24H or 12H format, depending on locale",
    "type": "text",
    "placeholders_order": [
      "medication",
      "time"
    ],
    "placeholders": {
      "medication": {},
      "time": {}
    }
  }
}