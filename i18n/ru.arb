{
  "@@last_modified": "2025-01-19T19:53:11.738830",
  "@@locale": "ru",
  "Enable notes on days without attacks": "Включить заметки на днях без атак",
  "@Enable notes on days without attacks": {
    "description": "Toggle button that enables (or disables) use of \"no headache\" as a \"strength\" option",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Lets you choose a \"no headache\" as the strength of an attack": "Позволяет выбрать «без головной боли» в качестве силы атаки",
  "@Lets you choose a \"no headache\" as the strength of an attack": {
    "description": "This is used as a description for the 'no headache' config option. Feel free to rephrase.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "No headache": "Нет головной боли",
  "@No headache": {
    "description": "Used as the 'strength' of a day where the user had no headache (for registering notes on days without symptoms)",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "General": "Основные",
  "@General": {
    "description": "Used as a header in the config screen to refer to \"general settings\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Continue": "Продолжить",
  "@Continue": {
    "description": "Used as a button in the 'first time' configuration screen",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Use neutral language": "Использовать нейтральный язык",
  "@Use neutral language": {
    "description": "Toggle button that enables (or disables) use of \"headache\" instead of \"migraine\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Use the term \"headache\" rather than \"migraine\"": "Использовать термин «головная боль» вместо «мигрень».",
  "@Use the term \"headache\" rather than \"migraine\"": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Enable \"aura only\" strength": "Включить силу «только аура»",
  "@Enable \"aura only\" strength": {
    "description": "Toggle button that enables (or disables) use of \"aura only\" as a \"strength\" option",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Let me set \"aura only\" as a the strength of an attack": "Позволяет выбрать «только аура» в качестве силы атаки",
  "@Let me set \"aura only\" as a the strength of an attack": {
    "description": "This is used as a description for the 'aura only' config option. Feel free to rephrase.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Show note indicators": "Отображать индикаторы заметок",
  "@Show note indicators": {
    "description": "Toggle button that enables/disables showing a marker on dates with notes in the calendar",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Show a small dot on dates where you have written a note": "Показывает небольшую точку на датах, где вы сделали заметку",
  "@Show a small dot on dates where you have written a note": {
    "description": "The 'dot' is a small grey square, displayed in the upper right hand corner of the date number in the calendar",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "What kind of headache do you have?": "Какая у вас головная боль?",
  "@What kind of headache do you have?": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Select this if you're unsure": "Выберите это, если не уверены",
  "@Select this if you're unsure": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine": "Мигрень",
  "@Migraine": {
    "description": "Used as level 2/3 (medium) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Other (general headaches, cluster etc.)": "Другая (обычная головная боль, кластерная и т.д.)",
  "@Other (general headaches, cluster etc.)": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Settings": "Настройки",
  "@Settings": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Configuration": "Конфигурация",
  "@Configuration": {
    "description": "The name of the first time configuration screen. This is pretty much like the settings screen, but with some different information and displayed only once, therefore using a different name to make this clearer.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medication list": "Список лекарств",
  "@Medication list": {
    "description": "Header for the part of the configuration screen where the user sets up their medication",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Changing the list of medications here will not change medications in entries you have already added.": "Изменение лекарств в этом списке никак не повлияет на лекарства в уже добавленных вами записях.",
  "@Changing the list of medications here will not change medications in entries you have already added.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "If you do not wish to add your medications, just press \"Continue\" and Migraine Log will add two generic ones for you to use.": "Если вы не хотите добавлять медикаменты, просто нажмите «Продолжить» и Дневник Мигрени добавит два непатентованных препарата, которые вы сможете использовать.",
  "@If you do not wish to add your medications, just press \"Continue\" and Migraine Log will add two generic ones for you to use.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Add a new medication": "Добавить новый медикамент.",
  "@Add a new medication": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Undo": "Отмена",
  "@Undo": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "_deletedElement": "«{medication}» удалён",
  "@_deletedElement": {
    "description": "A pop-up message after the user has deleted a medication. Is accompanied with an 'undo' button.",
    "type": "text",
    "placeholders_order": [
      "medication"
    ],
    "placeholders": {
      "medication": {}
    }
  },
  "No registration": "Нет записей",
  "@No registration": {
    "description": "Used in the pie diagram and table on the front page to denote days where no entries have been added. Avoid terminology like 'no headaches', since that might not be quite correct.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Headache": "Головная боль",
  "@Headache": {
    "description": "Used as level 1/3 (lowest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Mild headache": "Лёгкая головная боль",
  "@Mild headache": {
    "description": "Used as neutral level 1/3 (lowest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Moderate headache": "Умеренная головная боль",
  "@Moderate headache": {
    "description": "Used as neutral level 2/3 (medium) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Strong migraine": "Сильная мигрень",
  "@Strong migraine": {
    "description": "Used as level 3/3 (strongest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Strong headache": "Сильная головная боль",
  "@Strong headache": {
    "description": "Used as neutral level 3/3 (strongest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Aura only": "Только аура",
  "@Aura only": {
    "description": "Used as the 'strength' of a migraine or headache where the user only had aura symptoms",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine medication": "Лекарство от мигрени",
  "@Migraine medication": {
    "description": "Used as a generic 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Seizure medication": "Обезболивающее средство",
  "@Seizure medication": {
    "description": "Used as neutral generic 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Painkillers": "Болеутоляющее",
  "@Painkillers": {
    "description": "Used as the second 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Edit": "Изменить",
  "@Edit": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Register": "Записать",
  "@Register": {
    "description": "Displayed as the header when registering a new migraine attack",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "You must select a strength": "Вам нужно выбрать силу",
  "@You must select a strength": {
    "description": "Notice that appears if the user tries to save an entry without selecting a strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Save": "Сохранить",
  "@Save": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "editingEntryNewDate": "Вы изменяете запись на {editDate}. Если вы измените дату, то эта запись будет перенесена на новую дату.",
  "@editingEntryNewDate": {
    "type": "text",
    "placeholders_order": [
      "editDate"
    ],
    "placeholders": {
      "editDate": {}
    }
  },
  "Warning: there's already an entry on this date. If you save then the existing entry will be overwritten.": "Внимание: на этой дате уже имеется запись. Если вы сохраните, то существующая запись будет перезаписана.",
  "@Warning: there's already an entry on this date. If you save then the existing entry will be overwritten.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Date": "Дата",
  "@Date": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "change date": "изменить дату",
  "@change date": {
    "description": "Used as an action label on the button that triggers a calendar to select the date for an attack when a screen reader is used. It will be contextualized by the OS, on Android the OS will say the label of the button, then the word 'button', followed by 'double tap to change date'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Strength": "Сила",
  "@Strength": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Pain is very subjective. The only person that knows which option is right, is you. If you're uncertain, select the higher one of the two you're considering.": "Боль очень субъективна. Только вы можете знать, какой вариант для вас верен. Если вы сомневаетесь, выберите более сильную из двух рассматриваемых.",
  "@Pain is very subjective. The only person that knows which option is right, is you. If you're uncertain, select the higher one of the two you're considering.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Unable to perform most activities": "Невозможно выполнять большинство видов деятельности",
  "@Unable to perform most activities": {
    "description": "Migraine strength 3/3",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Unable to perform some activities": "Невозможно выполнять некоторые виды деятельности",
  "@Unable to perform some activities": {
    "description": "Migraine strength 2/3",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Able to perform most activities": "Возможно выполнять большинство видов деятельности",
  "@Able to perform most activities": {
    "description": "Migraine strength 1/3",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Aura symptoms only": "Только симптомы ауры",
  "@Aura symptoms only": {
    "description": "Migraine strength help text for aura only",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Close": "Закрыть",
  "@Close": {
    "description": "Used as a 'close' button in the help dialog for migraine strength. This will be converted to upper case when used in this context, ie. 'Close' becomes 'CLOSE'. This to fit with Android UI guidelines",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Which strength should I choose?": "Какую силу мне стоит выбрать?",
  "@Which strength should I choose?": {
    "description": "Used as the tooltip for the help button in the 'add' and 'edit' screens as well as the title of the help window that pops up when this button is pressed.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medications taken": "Принятые медикаменты",
  "@Medications taken": {
    "description": "Header in the add/edit window",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Note": "Заметка",
  "@Note": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "(none)": "(нет)",
  "@(none)": {
    "description": "Used in exported HTML when the user has taken no medications. It is an entry in a table where the header is 'Medications'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medications": "Медикаменты",
  "@Medications": {
    "description": "Used as a table header in exported data",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine Log": "Дневник Мигрени",
  "@Migraine Log": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "hide": "скрыть",
  "@hide": {
    "description": "Used in exported HTML as a link that hides one month. Will be displayed like '[hide]', so it should be lower case unless there are good reasons not to",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Hide this month": "Скрыть этот месяц",
  "@Hide this month": {
    "description": "Tooltip for a link that hides a month from view in the exported HTML",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "hidden:": "скрытые:",
  "@hidden:": {
    "description": "Will be rendered like: \"hidden: january 2021\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Total headache days": "Всего дней головной боли",
  "@Total headache days": {
    "description": "Used in a table, will render like 'Total headache days    20 days'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Taken medication": "Принимались лекарства",
  "@Taken medication": {
    "description": "Used in a table, will render like 'Taken Medication    5 days'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Filter:": "Фильтр:",
  "@Filter:": {
    "description": "Will be joined with the string 'show' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "show": "показать",
  "@show": {
    "description": "Will be joined with the string 'Filter:' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "everything": "всё",
  "@everything": {
    "description": "Will be joined with the string 'Filter: show' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "months": "месяцы",
  "@months": {
    "description": "Will be joined with a number, which will be 3 or higher, to become ie. '3 months'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "List days with no entries": "Отобразить дни без записей",
  "@List days with no entries": {
    "description": "Checkbox, toggles showing or hiding (default=hide) dates that have no registered headaches in the tables in our exported HTML file",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "_generatedMessage": "Сгенерировано приложением Дневник Мигрени версии {version}",
  "@_generatedMessage": {
    "type": "text",
    "placeholders_order": [
      "version"
    ],
    "placeholders": {
      "version": {}
    }
  },
  "Loading...": "Загрузка…",
  "@Loading...": {
    "description": "Used in exported HTML to indicate that we're loading the exported data",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Previous month": "Предыдущий месяц",
  "@Previous month": {
    "description": "Tooltip for the previous month button",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Next month": "Следующий месяц",
  "@Next month": {
    "description": "Tooltip for the next month button",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "_currentMonth": "Текущий месяц: {month}",
  "@_currentMonth": {
    "description": "Used for screen readers to describe the element in the month selector that indicates the current month. The month variable will contain a pre-localized month name along with the year",
    "type": "text",
    "placeholders_order": [
      "month"
    ],
    "placeholders": {
      "month": {}
    }
  },
  "Migraine Log helps you maintain a headache diary. Once you have added an entry, this screen will be replaced with statistics.\n\nAdd a new entry by pressing the \"+\"-button.\n\nFor more help, select \"Help\" in the menu at the top right of the screen.": "Дневник Мигрени поможет вам вести дневник головной боли. Как только вы добавите запись, этот экран будет заменён статистикой. \n\nДобавьте новую запись, нажав на кнопку «+»-.\n\nДля дополнительной информации, выберите кнопку «Помощь» в выпадающем меню в правом верхнем углу экрана.",
  "@Migraine Log helps you maintain a headache diary. Once you have added an entry, this screen will be replaced with statistics.\n\nAdd a new entry by pressing the \"+\"-button.\n\nFor more help, select \"Help\" in the menu at the top right of the screen.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Help": "Помощь",
  "@Help": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Icons in the calendar": "Иконки в календаре",
  "@Icons in the calendar": {
    "description": "Header in the help screen",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Colours": "Цвета",
  "@Colours": {
    "description": "Header in the help screen",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "An underline under a day in the calendar means that there is a registered entry on that date, but that no medication was taken.": "Нижнее подчёркивание под датой означает, что в этот день была зарегистрирована запись, но медикаменты не были приняты.",
  "@An underline under a day in the calendar means that there is a registered entry on that date, but that no medication was taken.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "A circle around a day in the calendar means that there is a registered entry on that date, and that medication was taken.": "Круг вокруг даты означает, что в этот день была зарегистрирована запись и были приняты медикаменты.",
  "@A circle around a day in the calendar means that there is a registered entry on that date, and that medication was taken.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "A dot in the upper right hand corner of a date indicates that the entry on that date includes a note.": "Точка в правом верхнем углу над датой означает, что запись включает в себя заметку.",
  "@A dot in the upper right hand corner of a date indicates that the entry on that date includes a note.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Throughout Migraine Log, colours are used to indicate the strength of a migraine. The underline/circle in the calendar, as well as the text of the strength when adding or editing an entry, will use a colour to signify the strength.": "В Дневнике Мигрени цвета используются для отображения силы мигрени. Нижнее подчёркивание/круг в календаре, а так же текст при добавлении и редактировании записи будут использовать цвет для обозначения силы.",
  "@Throughout Migraine Log, colours are used to indicate the strength of a migraine. The underline/circle in the calendar, as well as the text of the strength when adding or editing an entry, will use a colour to signify the strength.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "dayString": "{number,plural, =1{{number} день}=2{{number} дня}other{{number} дней}}",
  "@dayString": {
    "description": "How many days there are. This isn't as complicated as it looks. Unless you know you need to modify the pluralization rules (in which case see https://pub.dev/documentation/intl/latest/intl/Intl/plural.html) you should just change the string 'day' and 'days' here into your local equivalent. For instance, the Norwegian version of this string is: '{number,plural, =1{{number} dag}=2{{number} dagar}other{{number} dagar}}'. number will always be a positive integer (never zero).",
    "type": "text",
    "placeholders_order": [
      "number"
    ],
    "placeholders": {
      "number": {}
    }
  },
  "Successfully imported data": "Данные успешно импортированы",
  "@Successfully imported data": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "The file is corrupt and can not be imported": "Файл повреждён и не может быть импортирован",
  "@The file is corrupt and can not be imported": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "This file does not appear to be a Migraine Log (html) file": "Этот файл не похож на файл (html) Дневника Мигрени",
  "@This file does not appear to be a Migraine Log (html) file": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "This file is from a newer version of Migraine Log. Upgrade the app first.": "Этот файл сделан в более новой версии Дневника Мигрени. Сначала обновите приложение.",
  "@This file is from a newer version of Migraine Log. Upgrade the app first.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "An unknown error occurred during import.": "Во время импорта произошла неизвестная ошибка.",
  "@An unknown error occurred during import.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Import failed:": "Импорт не удался:",
  "@Import failed:": {
    "description": "Will contain information about why after the :",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Your Migraine Log version is too old to read the data you have. Please upgrade Migraine Log.": "Ваша версия Дневника Мигрени слишком устарела для того, чтобы прочитать ваши данные. Пожалуйста, обновите Дневник Мигрени.",
  "@Your Migraine Log version is too old to read the data you have. Please upgrade Migraine Log.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Your settings file failed to load. Your data is safe, and you may reset your settings to fix this issue (you will need to enter your medications again).": "Ваш файл настроек не был загружен. Ваши данные в порядке, но вам, возможно, придётся сбросить настройки, чтобы исправить эту ошибку (вам придётся заново внести ваши медикаменты).",
  "@Your settings file failed to load. Your data is safe, and you may reset your settings to fix this issue (you will need to enter your medications again).": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "If you want to report this issue, take a screenshot of the following information:": "Если вы хотите сообщить об этой проблеме, сделайте скриншот данной информации:",
  "@If you want to report this issue, take a screenshot of the following information:": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Reset settings": "Сбросить настройки",
  "@Reset settings": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Error": "Ошибка",
  "@Error": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.": "Дневник Мигрени — это бесплатное программное обеспечение: вы можете распространять и/или модифицировать его по условиям GNU General Public License, опубликованной Free Software Foundation, либо третьей версии, либо (на ваше усмотрение) самой последней.\n\nЭта программа распространяется с надеждой на то, что она будет полезна, но БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ; даже без подразумеваемых гарантий ТОВАРНОЙ ПРИГОДНОСТИ или ПРИГОДНОСТИ ДЛЯ ЧАСТНЫХ ЦЕЛЕЙ.",
  "@Migraine Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Copyright ©": "Авторское право ©",
  "@Copyright ©": {
    "description": "Will be followed by the author name and copyright year, which will be a link",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "See the": "Посмотрите",
  "@See the": {
    "description": "Used to construct the sentence 'See the GNU General Public License for details', where 'GNU General Public License' will be a link. Spacing around 'GNU General Public License' is automatic.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "for details.": "для подробностей.",
  "@for details.": {
    "description": "Used to construct the sentence 'See the GNU General Public License for details', where 'GNU General Public License' will be a link. Spacing around 'GNU General Public License' is automatic.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "About": "О приложении",
  "@About": {
    "description": "Menu entry for triggering the about dialog",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Exit": "Выход",
  "@Exit": {
    "description": "Used to exit the app",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Export": "Экспорт",
  "@Export": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Import": "Импорт",
  "@Import": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Statistics": "Статистика",
  "@Statistics": {
    "description": "Tooltip for the statistics tab",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Calendar": "Календарь",
  "@Calendar": {
    "description": "Tooltip for the calendar tab",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Home": "Домой:",
  "@Home": {
    "description": "Tooltip for the home tab",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Translated by": "Переведено",
  "@Translated by": {
    "description": "Should be a literal translation of this phrase, the value of TRANSLATED_BY will be appended to make a string like this: \"Translated by: TRANSLATED_BY\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "TRANSLATED_BY": "Сообществом Дневника Мигрени",
  "@TRANSLATED_BY": {
    "description": "Should be an alphabetical list of the names of the translators of this language, delimited with commas or the localized equivalent of \"and\". Will be displayed in the about dialog like this: \"Translated by: TRANSLATED_BY\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "deletedDateMessage": "Удалено {fmtDate}",
  "@deletedDateMessage": {
    "type": "text",
    "placeholders_order": [
      "fmtDate"
    ],
    "placeholders": {
      "fmtDate": {}
    }
  },
  "Delete": "Удалить",
  "@Delete": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Add": "Добавить",
  "@Add": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Show action menu": "Показать меню действий",
  "@Show action menu": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "lastNDaysMessage": "Последние {days} дней",
  "@lastNDaysMessage": {
    "description": "days should always be >1, so plural",
    "type": "text",
    "placeholders_order": [
      "days"
    ],
    "placeholders": {
      "days": {}
    }
  },
  "took no medication": "не принимались медикаменты",
  "@took no medication": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "tookMedsMessage": "принимался {meds}",
  "@tookMedsMessage": {
    "description": "This will expand into a list of medications taken, and will be added to the type of headache the user had. For instance, this might get 'Imigran' as meds, this will then be 'took Imigran', and it might expand into the sentece 'Migraine, took Imigran'",
    "type": "text",
    "placeholders_order": [
      "meds"
    ],
    "placeholders": {
      "meds": {}
    }
  },
  "Note:": "Заметка:",
  "@Note:": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Register medications": "Зарегистрировать медикаменты",
  "@Register medications": {
    "description": "Header in the add/edit window",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medication": "Медикаменты",
  "@Medication": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Taken": "Принято",
  "@Taken": {
    "description": "This is used in the header of the 'list of medications' table in the viewer. The column it refers to will contain the time that a medication was taken",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "takenAt": "Принят {medication} в {time}",
  "@takenAt": {
    "description": "medication is the name of one of the users medications, time is a time in either 24H or 12H format, depending on locale",
    "type": "text",
    "placeholders_order": [
      "medication",
      "time"
    ],
    "placeholders": {
      "medication": {},
      "time": {}
    }
  }
}